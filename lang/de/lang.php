<?php return [
    'plugin' => [
        'name' => 'Backend Benachrichtigungen',
        'description' => 'Stellt eine API zum Anzeigen von Benachrichtigungen im Backend bereit.',
    ],
    'permissions' => [
        'manage_notifications' => 'Benachrichtigungen verwalten',
    ],
    'notification' => 'Benachrichtigung',
    'notifications' => 'Benachrichtigungen',
    'notifications_counter_label' => [
        'singular' => 'Du hast einen neue Benachrichtigung.',
        'plural' => 'Du hast :count neue Benachrichtigungen',
    ],
    'manage_notifications' => 'Benachrichtigungen verwalten',
    'subject' => 'Betreff',
    'link' => 'Link',
    'body' => 'Text',
    'back' => 'Zurück',
    'mark_as_read' => 'als gelesen markieren',
    'mark_all_as_read' => 'alle als gelesen markieren',
    'mark_all_as_read_confirm' => 'Alle Benachrichtigung als gelesen markieren?',
    'no_notifications_message' => 'Du hast keine ungelesenen Benachrichtigungen.',
    'enable_notifications' => 'Benachrichtigungen aktivieren',
    'enable_notifications_text' => 'Bitte aktivieren Sie die Benachrichtigungen.',
    'preferences' => 'Benachrichtigungseinstellungen',
    'show_count' => 'Anzahl ungelesener Einträge anzeigen.',
    'enable_browser_push_notifications' => 'Pop up Benachrichtigungen auf meinem Computer anzeigen, wenn das Backend geöffnet ist.',
];
