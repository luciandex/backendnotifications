<?php return [
    'plugin' => [
        'name' => 'Backend Notifications',
        'description' => 'Adds API to display notifications in the backend.',
    ],
    'permissions' => [
        'manage_notifications' => 'Manage own notifications',
    ],
    'notification' => 'Notification',
    'notifications' => 'Notifications',
    'notifications_counter_label' => [
        'singular' => 'You have one new notification',
        'plural' => 'You have :count new notifications',
    ],
    'manage_notifications' => 'Manage notifications',
    'subject' => 'Subject',
    'link' => 'Link',
    'body' => 'Body',
    'back' => 'Back',
    'mark_as_read' => 'mark as read',
    'mark_all_as_read' => 'mark all as read',
    'mark_all_as_read_confirm' => 'Mark all notifications as read?',
    'no_notifications_message' => 'You have no unread notifications.',
    'enable_notifications' => 'Activate notifications',
    'enable_notifications_text' => 'Please activate the notifications.',
    'preferences' => 'Notification preferences',
    'show_count' => 'Show the number of unread items',
    'enable_browser_push_notifications' => 'Pop up notifications on my computer when the backend is open',

    'buttons' => [
        'save' => 'Save',
        'save_and_close' => 'Save and close',
        'or' => 'or',
        'cancel' => 'cancel',
        'delete' => 'Delete',
        'reset_default' => 'Reset to default',
    ],
];
