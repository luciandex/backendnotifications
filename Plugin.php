<?php namespace StudioBosco\BackendNotifications;

use Block;
use Backend;
use Lang;
use Event;
use Log;
use Config;
use Carbon\Carbon;
use System\Classes\PluginBase;
use StudioBosco\BackendNotifications\Models\Notification;
use StudioBosco\BackendNotifications\Helpers\BackendNotifications;

/**
 * BackendNotifications Plugin Information File
 */
class Plugin extends PluginBase
{
    use \System\Traits\ViewMaker;

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => Lang::get('studiobosco.backendnotifications::lang.plugin.name'),
            'description' => Lang::get('studiobosco.backendnotifications::lang.plugin.description'),
            'author'      => 'Ondrej Brinkel <ondrej@studiobosco.de>',
            'icon'        => 'icon-bell',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $self = $this;
        $this->addViewPath(plugins_path() . '/studiobosco/backendnotifications/partials');
        // TODO: check if we are in backend
        $pluginUrl = url('/plugins/studiobosco/backendnotifications');
        Block::append('body', $this->makePartial('ask_for_notification_permission'));
        Block::append('body', '<script type="text/javascript" src="' . $pluginUrl . '/assets/js/backendnotifications.js"></script>');

        Event::listen(
            'studiobosco.backendnotifications.notify',
            function ($user, string $subject, string $body = null, string $url = null) {
                try {
                    BackendNotifications::notify($user, $subject, $body, $url);
                } catch (\Exception $ex) {
                    Log::error($ex);
                }
            }
        );

        // remove notificaions older then 2h at login
        Event::listen(
            'backend.user.login',
            function ($user) use ($self) {
                $self->removeOldNotifications();
            }
        );
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'studiobosco.backendnotifications.manage_notifications' => [
                'label' => Lang::get('studiobosco.backendnotifications::lang.permissions.manage_notifications'),
                'tab' => Lang::get('studiobosco.backendnotifications::lang.plugin.name'),
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        $preferences = BackendNotifications::getPreferences();
        $count = $preferences->enable_notifications ? BackendNotifications::getCount() : 0;

        if (!$preferences->show_count && $count > 0) {
            $count = '+';
        }

        return [
            'backendnotifications' => [
                'label' => Lang::get('studiobosco.backendnotifications::lang.notifications'),
                'url' => Backend::url('studiobosco/backendnotifications/notifications'),
                'icon' => 'icon-bell',
                'permissions' => ['studiobosco.backendnotifications.manage_notifications'],
                'order' => 500,
                'counter' => $count,
                'counterLabel' => Lang::get(
                    'studiobosco.backendnotifications::lang.notifications_counter_label.' . ($count === 1 ? 'singular' : 'plural'),
                    ['count' => $count]
                ),
            ],
        ];
    }

    protected function removeOldNotifications()
    {
        // no neet do check if plugin was not installed yet
        if (!BackendNotifications::isInstalled()) {
            return;
        }

        $deleteSince = new Carbon();
        $deleteSince->subHours(2);
        $deleteSince->tz = Config::get('app.timezone');

        Notification::isRead()
        ->where('read_at', '<', $deleteSince)
        ->delete();
    }
}
