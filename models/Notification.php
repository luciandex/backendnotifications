<?php namespace StudioBosco\BackendNotifications\Models;

use Model;
use BackendAuth;
use Carbon\Carbon;

/**
 * Notification Model
 */
class Notification extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'studiobosco_backendnotifications_notifications';

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'user_id',
        'subject',
        'body',
        'url',
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'read_at',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => 'Backend\Models\User',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeListBackend($query)
    {
        $user = BackendAuth::getUser();

        return $query
        ->where('user_id', $user->id)
        ->isUnread()
        ->orderBy('created_at', 'desc');
    }

    public function scopeIsRead($query)
    {
        return $query->where('read_at', '!=', null);
    }

    public function scopeIsUnread($query)
    {
        return $query->where('read_at', null);
    }

    public function read()
    {
        $this->read_at = new Carbon();
        $this->save();
    }
}
