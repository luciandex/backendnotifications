(function () {
  let lastPolledAt = new Date();
  let preferences;

  function hasAskedForNotificationPermission() {
    return (window['Notification'] && window.Notification['permission'] !== 'default')
    ? true : false;
  }

  function checkNotificationPromise() {
    try {
      Notification.requestPermission().then();
    } catch(e) {
      return false;
    }

    return true;
  }

  function askNotificationPermission() {
    // function to actually ask the permissions
    function handlePermission(permission) {
      // Whatever the user answers, we make sure Chrome stores the information
      if (!('permission' in Notification)) {
        Notification.permission = permission;
      }
    }

    // Let's check if the browser supports notifications
    if (!('Notification' in window)) {
      console.log("This browser does not support notifications.");
    } else {
      if (checkNotificationPromise()) {
        Notification.requestPermission()
        .then((permission) => {
          handlePermission(permission);
        })
      } else {
        Notification.requestPermission(function(permission) {
          handlePermission(permission);
        });
      }
    }
  }

  function initPopup() {
    const btn = document.getElementById('studiobosco-backendnotifications-ask-notification-permission-button');
    if (btn) {
      btn.onclick = function() {
        askNotificationPermission();
        closePopup();
      };
    }
    const popup = document.getElementById('studiobosco-backendnotifications-ask-notification-permission-popup');
    if (popup) {
      openPopup();
      popup.querySelector('button[data-dismiss="modal"]').onclick = closePopup;
    }

    function openPopup() {
      popup.classList.add('in');
      popup.setAttribute('aria-hidden', 'false');
      popup.style.display = 'block';
    }

    function closePopup() {
      popup.classList.remove('in');
      popup.setAttribute('aria-hidden', 'true');
      popup.style.display = 'none';
    }
  }

  function handleError(err) {
    console.error(err);
  }

  function loadPreferences() {
    return fetch('/backend/studiobosco/backendnotifications/api/preferences', {
      header: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
    .then((res) => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error('Invalid response');
      }
    }, handleError)
    .then((res) => {
      return res.data;
    });
  }

  function loadCount() {
    return fetch('/backend/studiobosco/backendnotifications/api/count?t=' + (new Date().getTime()), {
      header: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
    .then((res) => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error('Invalid response');
      }
    }, handleError)
    .then((res) => {
      let count = res.data;
      const counterElement = document.querySelector('.counter[data-menu-id="backendnotifications"]');

      if (!preferences.show_count && count > 0) {
        count = '+';
      }

      if (counterElement) {
        counterElement.innerHTML = count ? count : '';

        if (count) {
          counterElement.classList.remove('empty');
        } else {
          counterElement.classList.add('empty');
        }
      }
    }, handleError);
  }

  function loadLatest() {
    const date = lastPolledAt.toISOString();
    lastPolledAt = new Date();
    return fetch('/backend/studiobosco/backendnotifications/api/latest?date=' + encodeURIComponent(date), {
      header: {
        'Content-Type': 'application/json'
      },
      credentials: 'include'
    })
    .then((res) => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error('Invalid response');
      }
    }, handleError)
    .then((res) => {
      return res.data;
    }, handleError);

  }

  function pollForNew() {
    loadCount();

    if (!preferences.enable_browser_push_notifications) {
      return;
    }

    loadLatest()
    .then((items) => {
      items.forEach((item) => {
        const notification = new Notification(item.subject, {
          body: item.body || ''
        });
        notification.onclick = function () {
          if (item.url) {
            window.location.href = item.url;
          }
        };
      });
    }, handleError)
    .catch(handleError);
  }

  loadPreferences()
  .then((_preferences) => {
    preferences = _preferences;
    if (!preferences.enable_notifications) {
      return;
    }

    setInterval(pollForNew, 5000);

    if (preferences.enable_browser_push_notifications && !hasAskedForNotificationPermission()) {
      initPopup();
    }
  });
})();
